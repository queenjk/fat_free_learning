<?php

/**
* This is a class to enable a user to be search the member table.
* It is a subclass of the controller class.
**/

class SearchController extends Controller
{

	// Set and get the search page for the surname of the member.
	public function showSearch($f3)
	{
		$f3->mset(
				array(
					'page'=>'views/search/search.php',
					'pagename'=>'Search for a member'
				)
			);
	}

	// Get the user response from the member surname search of the scrabble.member table.
	public function searchSurname($f3)
	{
		// Set the search page variables
		$f3->mset(
			array(
				'page'=>'views/search/search.php',
				'pagename'=>'Search results'
			)
		);

		// If nothing is posted, alert the user the fields are required and stop there
		if(empty($f3->get('POST.last_name')))
		{
			$f3->mset(
					array(
						'error.message'=>'Surname is a required field.'
					)
				);
			return;
		}

		// Something is posted to search surname form.
		else
		{
			// Get the name entered in to the form and cleanse the input anyway.
			$f3->set('name',$f3->clean($f3->get('POST.last_name')));

			// Run the name through the database and see if that gets a hit
			$f3->set('result',
				$this->db->exec(
					array('SELECT * FROM member WHERE LOWER(last_name)=:name'),
					array(array(':name'=>$f3->get('name')))
					)
			);

			/*Change the date format for the template here
			foreach($f3->get('result') as $item)
			{
				$f3->set('result.date_joined', date('d/m/Y H:i',strtotime($item['date_joined'])));
			}
			*/

			// If no results, set some variables and stop there.
			if(empty($f3->get('result')))
			{
				// Set the home page
				$f3->mset(
					array( 
						'results.message'=>"Sorry, no results found for '{$f3->get('name')}'"
					)
				);
				return;
			}
		}

	}

	// Process the response from the add member form when input is received.
	public function addUserToTheDatabase($f3)
	{
		// Set the add user form page as we're staying there.
		$f3->mset(
				array(
					'page'=>'views/search/adduser.php',
					'pagename'=>'Add a member to the table'
				)
			);

	
		// Now check all the fields are there and either enter the fields to the database or prompt the user to complete the form. 

		/** Call the submittedFieldCheck() function to check all the form fields are there.
		/* All the fields are there as the array should be empty so add the fields to the database.
		**/
		if(empty($this->submittedFieldCheck($f3)))
		{

			$f3->set('fields', $f3->clean($f3->get('POST')));
			// Add the form fields to the members table
			$this->addToMembersTable($f3);
		}

		// Not all the fields were completed so stop and let the user know.
		else
		{
			// Implode the missing fields to string and append to the message.
			$fields = implode(', ', str_replace('_', ' ', $this->fields));
			$f3->mset(
					array(
						'error.message' =>true,
						'error.fields'=>'These fields need completing: ' . $fields
					)
				);
			return;
		}

	}
	
	
}