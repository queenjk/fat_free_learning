<?php

/**
* This is a class to enable a user to be add, edit and delete a member from the member table.
* It is a subclass of the controller class.
**/

class EditController extends Controller
{

	// Get the add user form so the user can add a member to the scrabble.membefr table
	public function addUserForm($f3)
	{
		// Set the add member search form.
		$f3->mset(
				array(
					'page'=>'views/search/adduser.php',
					'pagename'=>'Add a member to the table'
				)
			);
	}

	// Process the response from the add member form when input is received.
	public function addUserToTheDatabase($f3)
	{
		// Set the add user form page as we're staying there.
		$f3->mset(
				array(
					'page'=>'views/search/adduser.php',
					'pagename'=>'Add a member to the table'
				)
			);

	
		// Now check all the fields are there and either enter the fields to the database or prompt the user to complete the form. 

		/** Call the submittedFieldCheck() function to check all the form fields are there.
		/* All the fields are there as the array should be empty so add the fields to the database.
		**/
		if(empty($this->submittedFieldCheck($f3)))
		{
			// Put the form fields in to the fields array
			$f3->set('fields', $f3->clean($f3->get('POST')));

			// Check that the phone number provider is numeric and is no longer than 11 digits
			if($this->checkPhoneNumber($f3)==false) return;

			// Add the form fields to the members table
			$this->addToMembersTable($f3);
		}

		// Not all the fields were completed so stop and let the user know.
		else
		{
			// Implode the missing fields to string and append to the message.
			$fields = implode(', ', str_replace('_', ' ', $this->fields));
			$f3->mset(
					array(
						'error.message' =>true,
						'error.fields'=>'These fields need completing: ' . $fields
					)
				);
			return;
		}
	}

	// Add the fields to the members table
	public function addToMembersTable($f3)
	{
		// Establish the table connection for members
		$this->member=new DB\SQL\Mapper($this->db,'member');
		
		// Add the record of the member to the scrabble.member table
		$this->member->first_name=ucfirst(strtolower($f3->get('fields.first_name')));
		$this->member->last_name=ucfirst(strtolower($f3->get('fields.last_name')));
		$this->member->date_joined=date('Y-m-d H:i:s');
		$this->member->contact_number=$f3->get('fields.contact_number');
		$this->member->enabled=1;
		$this->member->save();
		# Reset the data mapper so we can use again and add another record.
		$this->member->reset();

		// Set the message to display once the member record has been added.
		$f3->mset(
				array(
					'success.message' =>'The record was added successfully.'
				)
		);
		//return;
	}

	// Show the table so the user records can be edited.
	public function editMember($f3)
	{
		// Member selected via the member id, and isn't an after deletion and recall of the table results.
		if(is_numeric($f3->get('PARAMS.member_id')) && !$f3->get('showTable'))
		{
			// Set the page for the form to display with the edit member options.
			$f3->mset(
				array(
					'page'=>'views/edit/form.php',
					'pagename'=>'Edit the member details here'
				)
			);

			// Get and sanitise the member id from the URL.
			$member_id = $f3->clean($f3->get('PARAMS.member_id'));
		    
		    // Pull the selected member record from the members table.
		    $f3->set('result',$this->db->exec(
				array('SELECT * FROM member WHERE enabled=:enabled AND member_id=:member_id'),
				array(array(':enabled'=>1, 'member_id'=>$member_id))
				)
			);
		}

		// No Member selected, so get and show all the member records
		else
		{
			// Set the page for the form to display with the edit member options.
			$f3->mset(
				array(
					'page'=>'views/edit/table.php',
					'pagename'=>'Edit the member details here'
				)
			);

			// Pull all the enabled results from the members table.
			$f3->set('result',$this->db->exec(
				array('SELECT * FROM member WHERE enabled=:enabled ORDER BY date_joined DESC'),
				array(array(':enabled'=>1))
				)
			);
		}
	}

	public function updateMember($f3)
	{
		// Set the page for the form to display again, regardless.
		$f3->mset(
			array(
				'page'=>'views/edit/form.php',
				'pagename'=>'Edit the member details here'
			)
		);

		// The fields are not blank and all completed so sanitise and update the record.
		if(empty($this->submittedFieldCheck($f3)))
		{
			// Get the member id from the URL POSTED and stick in a var.
			$f3->set('member_id',$f3->get('PARAMS.member_id'));

			// Sanitise both the member id and the form fields posted for the update.
			$f3->set('fields',$f3->clean($f3->get('POST')));
			$f3->set('member_id',$f3->clean($f3->get('PARAMS.member_id')));

			// Establish the table connection for members as an object
			$this->member=new DB\SQL\Mapper($this->db,'member');
			// Load the member record in to the mapper.
			$this->member->load(array('member_id=?', $f3->get('member_id')));
			//print_r($this->member);return false;

			// Check that the phone number provider is numeric and is no longer than 11 digits
			if($this->checkPhoneNumber($f3)==false) return;
			
			// Update the record of the member to the scrabble.member table
			$this->member->first_name=$f3->get('fields.first_name');
			$this->member->last_name=$f3->get('fields.last_name');
			$this->member->contact_number=$f3->get('fields.contact_number');
			$this->member->updated_on=date('d/m/y H:i:s');
			$this->member->enabled=1;
			$this->member_id=$f3->get('member_id');
		
			$this->member->update();
			$this->member->save();

			// Let the user know the edit was successful
			/*
			$f3->mset(
			array(
				'success.message'=>$f3->get('fields.first_name').' '.$f3->get('fields.last_name'). '\'s record successfully updated in the member table'
				)
			);
			*/

			// Set this variable 'showTable' and pass it to the editMember($f3) function above, to recall all the results in the table.
			$f3->set('showTable', true);
			// Finally, call the function, get all the available results and pass the user to the members table.
			$this->editMember($f3);

		}

		else
		{
			// Implode the missing fields to string and append to the message.
			$fields = implode(', ', str_replace('_', ' ', $this->fields));
			$f3->mset(
					array(
						'error.message' =>true,
						'error.fields'=>'These fields need completing: ' . $fields
					)
				);
		}
	}

	// Delete the member record from the table.
	public function deleteMember($f3)
	{
		if(!empty($f3->get('PARAMS.member_id')))
		{
			// Get and sanitise the member id - just in case.
			$f3->set('member_id',$f3->clean($f3->get('PARAMS.member_id')));

			// Establish the table connection for members as an object of the mapper.
			$this->member=new DB\SQL\Mapper($this->db,'member');
			// Load the member record in to the mapper.
			$this->member->load(array('member_id=?', $f3->get('member_id')));

			// Delete the record from the member to the scrabble.member table
			/* NOT deleting, instead, just disabling: $this->member->erase();*/
			$this->member->enabled=0;
			$this->member->save();

			// Set this variable 'showTable' and pass it to the editMember($f3) function above, to recall all the results in the table.
			$f3->set('showTable', true);
			// Finally, call the function, get all the available results and pass the user to the members table.
			$this->editMember($f3);
		}

	}
	
}