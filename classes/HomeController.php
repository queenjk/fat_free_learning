<?php

/* HomeController.php just to handle the homepage which isn't doing much at the minute */

class HomeController extends Controller
{	
	// Set and get the app homepage - nothing there for now, just a brief summary.
	public function showIndex($f3)
	{
		$f3->mset(
				array(
					'page'=>'views/home/home.php',
					'pagename'=>'Homepage of members, where you can add, search and edit member records.',
					'pagetitle'=>'Homepage of members.'
				)
			);
	}
}