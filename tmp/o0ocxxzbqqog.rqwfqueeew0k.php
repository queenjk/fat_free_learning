<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $page_title; ?> - <?php echo $pagename; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/git_repos/fat_free_simple/ui/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/git_repos/fat_free_simple/ui/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/git_repos/fat_free_simple/ui/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/git_repos/fat_free_simple/ui/css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar">Home</span>
            <span class="icon-bar">About</span>
            <span class="icon-bar">Test</span>
          </button>
          <a class="navbar-brand" href="/git_repos/fat_free_simple/">Fat Free &amp; Bootstrap app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/git_repos/fat_free_simple/searchsurname">Search user</a></li>
            <li class="active"><a href="/git_repos/fat_free_simple/adduser">Add user</a></li>
            <li class="active"><a href="/git_repos/fat_free_simple/edituser">All users</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" role="main">
      <div class="jumbotron">
        <h1><?php echo $page_title; ?></h1>
        <h3><?php echo $pagename; ?></h3>
      </div>

      <!-- Do a require on the page we need -->
      <?php echo $this->render($page,$this->mime,get_defined_vars(),0); ?>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/git_repos/fat_free_simple/ui/css/js/bootstrap.min.js"></script>
    <script src="/git_repos/fat_free_simple/ui/css/js/docs.min.js"></script>
    <!-- IE10 viewpor<?php echo $this->render('header.htm',$this->mime,get_defined_vars(),0); ?>hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
