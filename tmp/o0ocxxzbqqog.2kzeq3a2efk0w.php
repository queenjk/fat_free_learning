<div class="row">
    <div class="col-md-12">
    	<h3>Please complete the form below to add a user to the members database.</h3>

        <?php if ($error['fields']): ?>
            
        	   <p style="padding:10px;" class="bg-primary"><?php echo $error['fields']; ?></p>
            
        <?php endif; ?>
        <?php if ($success['message']): ?>
            
             <p style="padding:10px;" class="bg-success"><?php echo $success['message']; ?></p>
            
        <?php endif; ?>
    </div>

    <div class="col-md-8">
        <form method="POST">
          <div class="form-group">
            <label for="name">Surname</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="" placeholder="Enter the Surname"/>
            <p style="float:right;"><span style="font-size:12px;padding:5px;background:red;color:#fff;">Required field</span></p>
          </div>
          <div class="form-group">
            <label for="name">Firstname</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="" placeholder="Enter the First name"/>
            <p style="float:right;"><span style="font-size:12px;padding:5px;background:red;color:#fff;">Required field</span></p>
          </div>
          <div class="form-group">
            <label for="name">Contact number</label>
            <input type="text" class="form-control" id="contact_number" name="contact_number" value="" placeholder="Enter the contact number"/>
            <p style="float:right;"><span style="font-size:12px;padding:5px;background:red;color:#fff;">Required field</span></p>
          </div>
        	<button type="submit" class="btn btn-default">Add the user</button>
        </form>
    </div>
</div>
