<div class="row">
	<div class="col-md-12">
		<h3>There are <?php echo count($result); ?> result(s) to view.</h3>

<p><a class="btn btn-primary" href="/git_repos/fat_free_simple/" role="button">Search again</a></p>

<table class="table table-striped table-bordered table-hover">
	<tr>
		<th>First name</th>
		<th>Last name</th>
		<th>Date joined</th>
		<th>Contact number</th> 
	</tr>
		<?php foreach (($result?:[]) as $item): ?>
			<tr>
				<td><span><?php echo $item['first_name']; ?></span></td>
				<td><span><?php echo $item['last_name']; ?></span></td>
				<td><span><?php echo date('d/m/Y H:i',strtotime($item['date_joined'])); ?></span></td>
				<td><span><?php echo $item['contact_number']; ?></span></td>
			</tr>
		<?php endforeach; ?>
</table>	    	
	</div>
</div>