<div class="row">
    <div class="col-md-12">
    	<h3>Please complete the form below to add a user to the members database.</h3>
    </div>

    <div class="col-md-8">
        <?php if ($error['fields']): ?>
            
        	   <p style="padding:10px;" class="bg-primary"><?php echo $error['fields']; ?></p>
            
        <?php endif; ?>
        <?php if ($success['message']): ?>
            
             <p style="color:#000;font-weight:bold;padding:10px;" class="bg-success"><?php echo $success['message']; ?></p>
            
        <?php endif; ?>

        <form method="POST">
          <div class="form-group">
            <label for="name">Surname</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $POST['last_name']; ?>" placeholder="Enter the Surname"/>
            <p><span class="required">Required field</span></p>
          </div>
          <div class="form-group">
            <label for="name">Firstname</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $POST['first_name']; ?>" placeholder="Enter the First name"/>
             <p><span class="required">Required field</span></p>
          </div>
          <div class="form-group">
            <label for="name">Contact number</label>
            <input type="text" class="form-control" id="contact_number" name="contact_number" value="<?php echo $POST['contact_number']; ?>" placeholder="Enter the contact number"/>
             <p><span class="required">Required field</span></p>
          </div>
        	<button type="submit" class="btn btn-primary">Add the user</button>
       </form>
    </div>
</div>
