<div class="row">
	<div class="col-md-8">
		<h3>Please complete the form below to access the members form.</h3>

<?php if ($error['message']): ?>
    
    	<p style="padding:10px;" class="bg-primary">Last name is a required field</p>
    
<?php endif; ?>
<?php if ($results['message']): ?>
    
    	<p style="padding:10px;" class="bg-primary">Sorry, no results found for '<?php echo $name; ?>'</p>
	
<?php endif; ?>

<form method="POST">
  <div class="form-group">
    <label for="name">Surname</label>
    <input type="text" class="form-control" id="last_name" name="last_name" value="" placeholder="Enter the surname of the member"/>
    <p style="float:right;"><span style="font-size:12px;padding:5px;background:red;color:#fff;">Required field</span></p>
  </div>
	<button type="submit" class="btn btn-default">Search</button>
</form>
	</div>
</div>
