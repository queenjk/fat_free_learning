<div class="row">
	<div class="col-md-12">
		<h4><span class="badge"> <?php echo count($result); ?> </span> result(s).</h4>

		<p><a class="btn btn-primary" href="/git_repos/fat_free_simple/searchSurname" role="button">Search again</a></p>

		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Date joined</th>
				<th>Contact number</th> 
			</tr>
				<?php foreach (($result?:[]) as $item): ?>
					<tr>
						<td><span><?php echo $item['first_name']; ?></span></td>
						<td><span><?php echo $item['last_name']; ?></span></td>
						<td><span><?php echo date('d/m/Y H:i\h\r\s',strtotime($item['date_joined'])); ?></span></td>
						<td><span><?php echo $item['contact_number']; ?></span></td>
					</tr>
				<?php endforeach; ?>
		</table>
		<p class="small">Scroll the table horizontally</p>    	
	</div>
</div>