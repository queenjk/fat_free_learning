
<?php if ($result): ?>
    
	    <div class="row">
	    <div class="col-md-12">
		<h3>There are <?php echo count($result); ?> result(s) to view.</h3>
<!-- Make the table responsive on mobile devices, namely mobiles -->
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover table-responsive">
	<tr>
		<th>First name</th>
		<th>Last name</th>
		<th>Date joined</th>
		<th>Contact number</th>
		<th>Updated</th>
		<th>&nbsp;&nbsp;</th> 
		<th>&nbsp;&nbsp;</th> 
	</tr>
		<?php foreach (($result?:[]) as $item): ?>
			<tr>
				<td><span><?php echo $item['first_name']; ?></span></td>
				<td><span><?php echo $item['last_name']; ?></span></td>
				<td><span><?php echo date('d/m/Y H:i\h\r\s',strtotime($item['date_joined'])); ?></span></td>
				<td><span><?php echo $item['contact_number']; ?></span></td>
				<td><span><?php echo ($item['updated_on']==NULL) ? "Hasn't been" : date('d/m/Y H:i\h\r\s',strtotime($item['updated_on'])); ?></span></td>
				<td valign="center"><span><a class="btn btn-primary" href="/git_repos/fat_free_simple/edituser/<?php echo $item['member_id']; ?>" role="button">Edit</a></span></td>
				<td valign="center"><span><a class="btn btn-danger" onclick='return confirm("Are you sure you want to delete this member?")' href="/git_repos/fat_free_simple/deleteuser/<?php echo $item['member_id']; ?>" role="button">Delete</a></span></td>
			</tr>
		<?php endforeach; ?>
</table>	    	
</div>
	</div>
</div>

<?php else: ?>
	<div class="row">
		<div class="col-md-12">
			<p>No results to display</p>
		</div>
	</div>

<?php endif; ?> 