<?php

// Kickstart the framework
$f3=require('lib/base.php');

// Load configuration file and the routes
$f3->config('config.ini');

// Set the debugging mode to full. 
$f3->set('DEBUG',1);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Run the framework instance.
$f3->run();
