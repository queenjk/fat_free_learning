<div class="row">
	<div class="col-md-12">
		<h3>Please complete the form below to access the members form.</h3>
    </div>

    <div class="col-md-8">
        <check if="{{ @error.message }}">
            <true>
            	<p style="padding:10px;" class="bg-primary">{{ @error.message }}</p>
            </true>
        </check>
        <check if="{{ @results.message }}">
            <true>
            	<p style="padding:10px;" class="bg-primary">{{ @results.message }}</p>
        	</true>
        </check>

        <form method="POST">
          <div class="form-group">
            <label for="name">Surname</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ @name }}" placeholder="Enter the surname of the member"/>
            <p><span class="required">Required field</span></p>
          </div>
        	<button type="submit" class="btn btn-primary">Search</button>
        </form>
	</div>
</div>
