
<check if="{{ @result }}">
    <true>
	    <div class="row">
	    <div class="col-md-12">
		<h3>There are {{count(@result)}} result(s) to view.</h3>
<!-- Make the table responsive on mobile devices, namely mobiles -->
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover table-responsive">
	<tr>
		<th>First name</th>
		<th>Last name</th>
		<th>Date joined</th>
		<th>Contact number</th>
		<th>Updated</th>
		<th>&nbsp;&nbsp;</th> 
		<th>&nbsp;&nbsp;</th> 
	</tr>
		<repeat group="{{ @result }}" value="{{ @item }}">
			<tr>
				<td><span>{{ @item.first_name  }}</span></td>
				<td><span>{{ @item.last_name  }}</span></td>
				<td><span>{{ date('d/m/Y H:i\h\r\s',strtotime(@item.date_joined)) }}</span></td>
				<td><span>{{ @item.contact_number }}</span></td>
				<td><span>{{ (@item.updated_on==NULL) ? "N/A" : date('d/m/Y H:i\h\r\s',strtotime(@item.updated_on))}}</span></td>
				<td valign="center"><span><a class="btn btn-primary" href="/git_repos/fat_free_learning/edituser/{{@item.member_id}}" role="button">Edit</a></span></td>
				<td valign="center"><span><a class="btn btn-danger" onclick='return confirm("Are you sure you want to delete this member?")' href="/git_repos/fat_free_learning/deleteuser/{{@item.member_id}}" role="button">Delete</a></span></td>
			</tr>
		</repeat>
</table>	    	
</div>
	</div>
</div>
</true>
<false>
	<div class="row">
		<div class="col-md-12">
			<p>No results to display</p>
		</div>
	</div>
</false>
</check> 